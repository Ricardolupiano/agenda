public class Pessoa{

//Atributos 
	private String nome;
	private String cpf;
	private String telefone;

//Construtor
	public Pessoa(){
	}
	public Pessoa(String umNome,String umTelefone){
		nome = umNome;
		telefone = umTelefone;
	}
	public Pessoa(String umNome,String umTelefone,String umCpf){
		nome = umNome;
		telefone = umTelefone;
		cpf = umCpf;
	}



//Metodos
	public void setNome(String umNome){
		nome = umNome;
	}
	public String getNome(){
		return nome;
	}
	public void setCpf(String umCpf){
		cpf = umCpf;
	}
	public String getCpf(){
		return cpf;
	}
	public void setTelefone(String umTelefone){
		telefone = umTelefone;
	}
    public String getTelefone(){
        return telefone;
    }
}
