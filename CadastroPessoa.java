import java.io.*;

public class CadastroPessoa{
	
	public static void main(String[] args) throws IOException{
		//burocracia para teclado
		InputStream entradaSistema = System.in;
		InputStreamReader leitor = new InputStreamReader(entradaSistema);
		BufferedReader leitorEntrada = new BufferedReader(leitor);
		String entradaTeclado;
		//Declarando algumas variaveis
		
		char menu = 'a';
        String umNome;
        String mensagem;
        String nomePessoa;
        String telefonePessoa;
        String cpfPessoa;
        String telefoneContato;

		//instanciando objetos do sistema
		ControlePessoa umControle = new ControlePessoa();
        Pessoa umaPessoa = new Pessoa();

		//interagindo com o usuário:
		
		while(menu != 'b'){
			System.out.println("===========MENU===========");
			System.out.println("1- Adicionar Pessoa");
			System.out.println("2- Remover Pessoa");
			System.out.println("3- Pesquisar Pessoa pelo Nome");
			System.out.println("4- Pesquisar Pessoa pelo Telefone");
			System.out.println("5- Sair");
			System.out.println("Entre com a opção desejada: ");
			entradaTeclado = leitorEntrada.readLine();
			menu = entradaTeclado.charAt(0);
			
			if(menu == '1'){
				System.out.println("Digite o nome da Pessoa: ");
				entradaTeclado = leitorEntrada.readLine();
				umNome = entradaTeclado;
				System.out.println("Digite o telefone da Pessoa: ");
				entradaTeclado = leitorEntrada.readLine();
				String umTelefone = entradaTeclado;
				System.out.println("Digite o cpf da Pessoa: ");
				entradaTeclado = leitorEntrada.readLine();
				String umCpf = entradaTeclado;	
				umaPessoa = new Pessoa(umNome,umTelefone,umCpf);
				mensagem = umControle.adicionar(umaPessoa);
				System.out.println(mensagem);
			}
			else if(menu == '2'){
				System.out.println("Digite o nome da Pessoa");
				entradaTeclado = leitorEntrada.readLine();
				umNome = entradaTeclado;
				umaPessoa = umControle.pesquisarNome(umNome);
				mensagem = umControle.remover(umaPessoa);
				System.out.println(mensagem);
			}
			else if(menu == '3'){
				System.out.println("Digite o nome da Pessoa: ");
				entradaTeclado = leitorEntrada.readLine();
				umNome = entradaTeclado;
				umaPessoa = umControle.pesquisarNome(umNome);
				nomePessoa = umaPessoa.getNome();
				telefonePessoa = umaPessoa.getTelefone();
				cpfPessoa = umaPessoa.getCpf();
				System.out.println("Nome: " + nomePessoa);
				System.out.println("Telefone: " + telefonePessoa);
				System.out.println("Cpf: " + cpfPessoa);
			}
			else if(menu == '4'){
				System.out.println("Digite o telefone da Pessoa: ");
				entradaTeclado = leitorEntrada.readLine();
				telefoneContato = entradaTeclado;
				umaPessoa = umControle.pesquisarTelefone(telefoneContato);
				nomePessoa = umaPessoa.getNome();
				cpfPessoa = umaPessoa.getCpf();
				telefonePessoa = umaPessoa.getTelefone();
				System.out.println("Nome: " + nomePessoa);
				System.out.println("Telefone: " + telefonePessoa);
				System.out.println("Cpf: " + cpfPessoa);
			}
			else if(menu == '5'){
				menu = 'b';
			}
			else{
				System.out.println("Opção inválida, tente novamente.");
			}
		}
	}
}


				
