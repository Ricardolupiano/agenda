import java.util.ArrayList;

public class ControlePessoa{

//Atributos
	private ArrayList<Pessoa> listaPessoas;
//construtor

	public ControlePessoa(){
		listaPessoas = new ArrayList<Pessoa>();
	}

//métodos

	public String adicionar(Pessoa umaPessoa){
		String mensagem = "Pessoa adicionada com sucesso!";
		listaPessoas.add(umaPessoa);
		return mensagem;
	}

	public String remover(Pessoa umaPessoa){
		String mensagem = "Pessoa removida com sucesso!";
		listaPessoas.remove(umaPessoa);
		return mensagem;
	}
	public Pessoa pesquisarNome(String umNome){
		for(Pessoa pessoa: listaPessoas){
			if(pessoa.getNome().equalsIgnoreCase(umNome)){
				return pessoa;
			}
		}
		return null;
	}
	public Pessoa pesquisarTelefone(String umNome){
		for(Pessoa pessoa: listaPessoas){
			if(pessoa.getTelefone().equalsIgnoreCase(umNome)){
				return pessoa;
			}
		}
		return null;
	}

	public void exibirContatos(){
		for(Pessoa pessoa: listaPessoas){
			System.out.println(pessoa.getNome());
		}
	}
}
